package com.example.proyectodeprueba;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProyectodepruebaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProyectodepruebaApplication.class, args);
	}

}
